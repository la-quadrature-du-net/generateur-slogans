#!/usr/bin/env node

const fs = require('fs');
const querystring = require('querystring');
const express = require('express');
const Canvas = require('canvas');
const Image = Canvas.Image;
const app = express();
global.config = require('./config.json');
const i18n = require('./i18n.js');
const ShareMessage = require('./shareMessage.js');

app.set('trust proxy', true);
app.set('x-powered-by', false);
app.set('view engine', 'pug');
app.use(express.static('./static'));

app.use(function(req, res, next) {
  let locale = req.query.changeLang || req.query.lang || req.headers['accept-language'] || '';
  req.i18n = new i18n();
  if (locale.indexOf('en') !== -1
      && locale.indexOf('fr') !== -1)
    req.i18n.setLocale(locale.indexOf('en') < locale.indexOf('fr') ? 'en' : 'fr');
  else if (locale.indexOf('en') !== -1
	   && locale.indexOf('fr') === -1)
    req.i18n.setLocale('en');
  else
    req.i18n.setLocale('fr');
  next();
});

app.use(function(req, res, next) {
  let _ = req.i18n._;
  let bgColors = ['#f54358', '#ee0088', '#ff9146', '#ffd201', '#39d0b7', '#2f5189', '#000'];
  let fgColors = ['#f54358', '#ee0088', '#ff9146', '#ffd201', '#39d0b7', '#2f5189', '#000', '#fff'];
  let sampleTexts = ["arachnides", "bicyclette", "bidule", "bilboquet", "biscuit", "bitcoins", "bitte", "bollard", "cantine", "casserole", "chausse-pied", "chaloupe", "chalutier", "champignons", "chignon", "chouchen", "couleurs moches", "coton-tige", "cucurbitacée", "culotte", "démocratie", "élastique", "enzyme", "fonte", "frigogidaire", "germanium", "gousse", "gousset", "hurluberlu", "hypocondrie", "loquet", "mammouth", "marmite", "Michel Sardou", "musaraigne", "narcotype", "ornythorinque", "palette", "parc de stationnement", "pelle à tarte", "perpendicularité", "plat à tarte", "poêle", "pogne", "poireau", "polochon", "pop-corn", "poulpe", "poutrelle", "prolétariat", "protozoaire", "rateau", "sabot", "sabre laser", "semi-remorque", "soulier", "taquet", "tartenpion", "tartignol", "téléportation", "tentacules", "tractopelle", "trou noir", "vérité", "zeste"];
  let sampleSignatures = ["\t", "moi\t", "les gens sympas\t", "les gens biens\t"];
  let _fonts = [];

  for (let i in fonts) {
    _fonts.push(i);
  }

  req.args = {
    bgColor: req.query.bgColor || '#f54358',
    fgColor: req.query.fgColor || '#ffd201',
    text: req.query.text ? req.query.text.trim() : 'démocratie',
    font: req.query.font || 'lineal',
    icon: req.query.icon || 1,
    signature: req.query.signature ? req.query.signature.trim() : ''
  };

  if (req.query.target === 'randomize') {
    req.args.bgColor = bgColors[Math.floor(Math.random() * bgColors.length)];
    do {
      req.args.fgColor = fgColors[Math.floor(Math.random() * fgColors.length)];
    } while (req.args.fgColor === req.args.bgColor);
    req.args.text = _(sampleTexts[Math.floor(Math.random() * sampleTexts.length)]);
    req.args.font = _fonts[Math.ceil(Math.random() * (_fonts.length - 1))];
    req.args.icon = Math.ceil(Math.random() * 36);
    req.args.signature = _(sampleSignatures[Math.floor(Math.random() * sampleSignatures.length)]);
  }

  next();
});

// {name: {filepath, offset}}
let fonts = {
  'FiraSans': {
    path: 'FiraSans-Regular.otf',
    offset: 0
  },
  'bilbo-inc': {
    path: 'BilboINC.ttf',
    offset: 10
  },
  'bluu-next': {
    path: 'BluuNext-Bold.otf',
    offset: 0
  },
  'combat': {
    path: 'Combat.otf',
    offset: 0
  },
  'gulax': {
    path: 'Gulax.otf',
    offset: 10
  },
  'lineal': {
    path: 'Lineal.otf',
    offset: 4
  },
  'resistance': {
    path: 'Resistance.otf',
    offset: 2
  },
  'savate': {
    path: 'savate-regular.otf',
    offset: 10
  },
  'solid-mirage': {
    path: 'SolideMirageMono.otf',
    offset: 0
  },
  'steps-mono': {
    path: 'Steps-Mono.otf',
    offset: 10
  },
  'terminal': {
    path: 'terminal-grotesque_open.otf',
    offset: 15
  }
};

for (let i in fonts) {
  Canvas.registerFont('./static/fonts/' + fonts[i].path, {family: i});
}
Canvas.registerFont('./static/fonts/FiraSans-Bold.otf', {family: 'FiraSans', weight: 'bold'});

app.get('/generate.png', function (req, res) {
  let _ = req.i18n._;

  let message = new ShareMessage(req);
  if (req.query.target === 'diaspora') {
    return res.redirect(message.getDiasporaLink())
  }
  else if (req.query.target === 'mastodon') {
    return res.redirect(message.getMastodonLink())
  }
  else if (req.query.target === 'facebook') {
    return res.redirect(message.getFacebookLink());
  }
  else if (req.query.target === 'twitter') {
    return res.redirect(message.getTwitterLink());
  }
  else if (req.query.target === 'download') {
    res.set("Content-Disposition", "attachment;filename=lqdn_slogan.png");
  }
  let width = 600;
  let height = 413;
  let fontSize = 44;
  let canvas = Canvas.createCanvas(width, height);
  let ctx = canvas.getContext('2d');

  // Clear canvas
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  // Draw background
  ctx.fillStyle = req.args.bgColor;
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.fill();

  // Write text
  ctx.font = fontSize + 'pt FiraSans';
  ctx.fillStyle = 'white';
  ctx.textBaseline = 'top';
  ctx.fillText(_('Internet,'), width * 0.08, height * 0.2);
  ctx.fillText(_('et libertés.'), width * 0.08, height * 0.2 + fontSize * 2.8);
  ctx.font = fontSize + 'pt ' + req.args.font;
  ctx.fillStyle = req.args.fgColor;
  ctx.fillText(req.args.text, width * 0.08, height * 0.2 + fontSize * 1.4 + fonts[req.args.font].offset);

  // Add footer
  ctx.fillStyle = 'white';
  ctx.fillRect(0, height * 0.85 - 1, width, height * 0.15 + 1);
  ctx.fill();
  ctx.font = '12pt FiraSans';
  ctx.fillStyle = '#497ed5';
  let offset = height * 0.05;
  if (req.args.signature.trim()) {
    ctx.fillText(_('Faites comme '), offset, height * 0.9);
    offset += ctx.measureText(_('Faites comme ')).width;
    ctx.font = 'bold 12pt FiraSans';
    ctx.fillText(req.args.signature.trim(), offset, height * 0.9);
    offset += ctx.measureText(req.args.signature.trim()).width;
    ctx.font = '12pt FiraSans';
    ctx.fillText(', s', offset, height * 0.9);
    offset += ctx.measureText(', s').width;
  } else {
    ctx.fillText('S', offset, height * 0.9);
    offset+= ctx.measureText('S').width;
  }
  ctx.fillText(_('outenez '), offset, height * 0.9);
  offset += ctx.measureText(_('outenez ')).width;
  ctx.font = 'bold 12pt FiraSans';
  ctx.fillText('La Quadrature du Net !', offset, height * 0.9);

  // Draw picto
  let img = new Image();
  let imgXml = fs.readFileSync('./static/imgs/' + req.args.icon + '.svg');
  imgXml = imgXml.toString('ascii').replace(/#FFD201/gi, req.args.fgColor);
  img.src = 'data:image/svg+xml;base64,' + new Buffer(imgXml).toString('base64');
  ctx.drawImage(img, 0, 0);

  // Render image
  let stream = canvas.pngStream();
  stream.on('data', function(chunk) {
    res.write(chunk);
  });

  stream.on('end', function() {
    res.end();
  })
});

app.get('/', function(req, res) {
  res.render('index', {protocol: req.protocol,
		       host: req.headers.host,
		       imageParams: querystring.stringify(req.args),
		       params: req.args,
		       subdir: config.subdir,
		       locale: req.i18n.getLocale(),
		       _: req.i18n._});
})

app.listen(process.env.PORT || config.port || 80,
	   process.env.HOST || config.host || 'localhost');
