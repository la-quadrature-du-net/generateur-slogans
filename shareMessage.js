const querystring = require('querystring');

module.exports = function(req) {
  let _req = req;
  let _ = req.i18n._;
  let _lqdnName = "@laquadrature";

  this.getUrl = function() {
    return _req.protocol + '://'
      + _req.headers.host + '/'
      + (global.config.subdir || '')
      + '?'
      + querystring.stringify(_req.args)
      + '&lang=' + _req.i18n.getLocale();
  }

  this.getTitle = function() {
    return (_req.args.signature ? _('Faites comme ') + _req.args.signature + ', s' : 'S')
      + _('outenez ')
      + _lqdnName + ' ! #LQDoN';
  }

  this.getContent = function() {
    return _req.protocol + "://" + _req.headers.host;
  }

  this.getTwitterLink = function() {
    _lqdnName = "@laquadrature";
    return 'https://twitter.com/intent/tweet?text='
      + encodeURIComponent(this.getTitle()
			   + ' ' + this.getContent()
			   + ' ' + this.getUrl());
  }

  this.getFacebookLink = function() {
    _lqdnName = "@laquadrature";
    return 'https://www.facebook.com/sharer/sharer.php?u='
      + encodeURIComponent(this.getUrl());
  }

  this.getDiasporaLink = function() {
    _lqdnName = "La Quadrature du Net";
    return 'https://share.diasporafoundation.org/?title='
      + encodeURIComponent(this.getTitle() + ' ' + this.getContent())
      + '&url=' + encodeURIComponent(this.getUrl());
  }

  this.getMastodonLink = function() {
    _lqdnName = "@laquadrature";
    return 'https://mamot.fr/share?text='
      + encodeURIComponent(this.getTitle()
			   + ' ' + this.getContent()
			   + ' ' + this.getUrl());
  }
}
