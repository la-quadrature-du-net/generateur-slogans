const enLocale = require('./locale/en.json');

module.exports = function(){
  let _locale = 'en';

  this._ = function(str) {
    return _locale == 'en' ? enLocale[str] || str : str;
  }

  this.setLocale = function(locale) {
    _locale = locale;
  }

  this.getLocale = function() {
    return _locale;
  }

  return this;
};
