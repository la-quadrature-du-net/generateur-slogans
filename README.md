# Slogans generator

This project is the slogan generator used by LQDN for the donation compaign of years 2017-2018.

## Installation

The back-end server require node.js (v6.11.X), npm and cairo. To daemonize the backend, we're going to use pm2.

Here is the installation command for debian based systems:

```sh
apt install nodejs libcairo2-dev libjpeg62-turbo-dev librsvg2-dev libpango1.0-dev libgif-dev build-essential g++
```

```sh
sudo npm install -g pm2
cp config.json.default config.json
npm install
```

## Run

We're suggesting the use of pm2 to manage the service
```sh
pm2 start slogan.app.yml
```
