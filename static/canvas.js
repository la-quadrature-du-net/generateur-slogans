let cachedImages = [];
let sampleTexts = ["arachnides", "bicyclette", "bidule", "bilboquet", "biscuit", "bitcoins", "bitte", "bollard", "cantine", "casserole", "chausse-pied", "chaloupe", "chalutier", "champignons", "chignon", "chouchen", "couleurs moches", "coton-tige", "cucurbitacée", "culotte", "démocratie", "élastique", "enzyme", "fonte", "frigogidaire", "germanium", "gousse", "gousset", "hurluberlu", "hypocondrie", "loquet", "mammouth", "marmite", "Michel Sardou", "musaraigne", "narcotype", "ornythorinque", "palette", "parc de stationnement", "pelle à tarte", "perpendicularité", "plat à tarte", "poêle", "pogne", "poireau", "polochon", "pop-corn", "poulpe", "poutrelle", "prolétariat", "protozoaire", "rateau", "sabot", "sabre laser", "semi-remorque", "soulier", "taquet", "tartenpion", "tartignol", "téléportation", "tentacules", "tractopelle", "trou noir", "vérité", "zeste"];
let sampleSignatures = ["\t", "moi\t", "les gens sympas\t", "les gens biens\t"];

let canvas = document.getElementById('output');
let ctx = canvas.getContext('2d');
let enLocale = {
  "Internet,": "Internet,",
  "et libertés.": "et libertés.",
  "Faites comme ": "Just like ",
  "outenez ": "upport ",
  "moi\t": "me\t",
  "les gens sympas\t": "nice people\t",
  "les gens biens\t": "cool people\t"
};

let width = canvas.width;
let height = canvas.height;
let fontSize = 44;

let params = {};
if (window.location.search) {
  let splitParams = window.location.search.split('?')[1].split('&');
  for (let i in splitParams) {
    params[splitParams[i].split('=')[0]] = decodeURIComponent(splitParams[i].split('=')[1].replace(/\+/g, ' '));
  }
}

let get = function(url, cb) {
  let xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
      cb(xmlHttp.responseText);
  }
  xmlHttp.open("GET", url, true);
  xmlHttp.send(null);
}

let getImage = function(id, cb) {
  if (localStorage.getItem('img:' + id))
    cb(localStorage.getItem('img:' + id));
  else {
    get('./imgs/' + id + '.svg', function(res) {
      localStorage.setItem('img:' + id, res);
      cb(res);
    });
  }
}

let language = function() {
  return document.documentElement.lang;
}

let _ = function(str) {
  return language() === 'en' ? enLocale[str] || str : str;
}

let draw = function() {
  let bgColor = document.querySelector('.colorpicker input[type="radio"][name="bgColor"]:checked').value;
  let fgColor = document.querySelector('.colorpicker input[type="radio"][name="fgColor"]:checked').value;
  let fillText = document.getElementById('step2value').value.trim();
  let font = document.querySelector('#step3 input[type="radio"]:checked').value;
  let icon = document.querySelector('input[name="icon"]:checked').value;
  let signature = document.getElementById('signature').value.trim();

  // Clear canvas
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  // Draw background
  ctx.fillStyle = bgColor;
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.fill();

  // Add footer
  ctx.fillStyle = 'white';
  ctx.fillRect(0, height * 0.85 - 1, width, height * 0.15 + 1);
  ctx.fill();
  ctx.font = '12pt FiraSans';
  ctx.textBaseline = 'top';
  ctx.fillStyle = '#497ed5';
  let offset = height * 0.05;
  if (signature) {
    ctx.fillText(_('Faites comme '), offset, height * 0.9);
    offset += ctx.measureText(_('Faites comme ')).width;
    ctx.font = 'bold 12pt FiraSans';
    ctx.fillText(signature, offset, height * 0.9);
    offset += ctx.measureText(signature).width;
    ctx.font = '12pt FiraSans';
    ctx.fillText(', s', offset, height * 0.9);
    offset += ctx.measureText(', s').width;
  } else {
    ctx.fillText('S', offset, height * 0.9);
    offset += ctx.measureText('S').width;
  }
  ctx.fillText(_('outenez '), offset, height * 0.9);
  offset += ctx.measureText(_('outenez ')).width;
  ctx.font = 'bold 12pt FiraSans';
  ctx.fillText('La Quadrature du Net !', offset, height * 0.9);

  // Draw picto
  getImage(icon, function(res) {
    let img = new Image();
    let imgXml = res.replace(/#FFD201/g, fgColor);
    img.src = 'data:image/svg+xml;base64,' + btoa(imgXml);
    img.addEventListener('load', function() {
      ctx.drawImage(img, 0, 0);

      // Write text
      ctx.font = fontSize + 'pt FiraSans';
      ctx.fillStyle = 'white';
      ctx.textBaseline = 'top';
      ctx.fillText(_('Internet,'), width * 0.08, height * 0.2);
      ctx.fillText(_('et libertés.'), width * 0.08, height * 0.2 + fontSize * 2.8);
      ctx.font = fontSize + 'pt ' + font;
      ctx.fillStyle = fgColor;
      ctx.fillText(fillText, width * 0.08, height * 0.2 + fontSize * 1.4);

      // Edit URL
      history.replaceState('', '', window.location.pathname + '?'
			   + 'bgColor=' + encodeURIComponent(bgColor)
			   + '&fgColor=' + encodeURIComponent(fgColor)
			   + '&text=' + encodeURIComponent(fillText)
			   + '&font=' + encodeURIComponent(font)
			   + '&icon=' + encodeURIComponent(icon)
			   + '&lang=' + encodeURIComponent(language())
			   + '&signature=' + encodeURIComponent(document.getElementById('signature').value)
			   + window.location.hash);
    }, false);
  });
}

let navElems = document.querySelectorAll('nav > a');
for (let i = 0; i < navElems.length; ++i) {
  navElems[i].onclick = function(e) {
    for (let j = 0; j < navElems.length; ++j) {
      navElems[j].classList.remove('active-tab');
    }
    e.target.classList.add('active-tab');
  }
}

let navButtons = document.querySelectorAll('.navButtons > a[href^="#"]');
for (let i = 0; i < navButtons.length; ++i) {
  navButtons[i].onclick = function(e) {
    document.querySelector('nav > a[href="' + e.target.attributes.href.value + '"]').click();
  }
}

window.addEventListener('load', function() {
  // Display correct tab
  let hash = window.location.hash || '#step1';
  document.querySelector('nav > a[href="#step3"]').click();
  document.querySelector('nav > a[href="' + hash + '"]').click();

  // Update inputs depending on URL params
  if (params['bgColor'])
    document.querySelector('.colorpicker input[name="bgColor"][value="' + params['bgColor'] + '"]').checked = true;
  if (params['fgColor'])
    document.querySelector('.colorpicker input[name="fgColor"][value="' + params['fgColor'] + '"]').checked = true;
  if (params['text'])
    document.getElementById('step2value').value = document.getElementById('step2value').innerText = params['text'];
  if (params['font'])
    document.querySelector('input[name="font"][value="' + params['font'] + '"]').checked = true;
  if (params['icon'])
    document.querySelector('input[name="icon"][value="' + params['icon'] + '"]').checked = true;
  if (params['signature'])
    document.getElementById('signature').value = params['signature'];

  // Prevent user from selecting two identical colors
  let inputs = document.querySelectorAll('.colorpicker input[type="radio"]');
  for (let i = 0; i < inputs.length; ++i) {
    inputs[i].addEventListener('click', function(e) {
      if (document.querySelector('.colorpicker input[type="radio"][name="bgColor"]:checked').value
	  == document.querySelector('.colorpicker input[type="radio"][name="fgColor"]:checked').value)
	e.preventDefault();
    });
  }

  // Add event listeners to inputs
  inputs = document.querySelectorAll('.colorpicker input[type="radio"], #step3 input[type="radio"], input[type="radio"][name="icon"]');
  for (let i = 0; i < inputs.length; ++i) {
    inputs[i].addEventListener('change', draw);
  }
  document.getElementById('step2value').oninput = draw;
  document.getElementById('signature').oninput = draw;

  // Remove server calls for random buttons
  let buttons = document.querySelectorAll('button.randomize');
  for (let i = 0; i < buttons.length; ++i) {
    buttons[i].type = "button";
  }

  // Update canvas
  document.fonts.onloadingdone = function(fontFaceSetEvent) {
    draw();
  }
});

let randomElement = function(selector) {
  let elems = document.querySelectorAll(selector);
  return elems[Math.trunc(Math.random() * elems.length)];
}

let randomButtons = document.querySelectorAll('.randomize');
for (let i = 0; i < randomButtons.length; ++i) {
  randomButtons[i].onclick = function(e) {
    randomElement('input[name="bgColor"]').checked = true;
    do {
      randomElement('input[name="fgColor"]').checked = true;
    } while (document.querySelector('.colorpicker input[type="radio"][name="bgColor"]:checked').value
	     == document.querySelector('.colorpicker input[type="radio"][name="fgColor"]:checked').value);
    randomElement('input[name="font"]').checked = true;
    randomElement('#step4 input[name="icon"]').checked = true;
    document.getElementById('step2value').value = document.getElementById('step2value').innerText = _(sampleTexts[Math.trunc(Math.random() * sampleTexts.length)]);
    if (document.getElementById('signature').value.match(/(\t$|^$)/))
      document.getElementById('signature').value = _(sampleSignatures[Math.trunc(Math.random() * sampleSignatures.length)]);
    draw();
  }
}
