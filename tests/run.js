#!/usr/bin/env nodeunit

global.config = {};
const I18n = require('../i18n.js');
const ShareMessage = require('../shareMessage.js');
const i18n = new I18n();

exports.i18n = {
  defaultLocale: function(test) {
    test.strictEqual(i18n.getLocale(), 'en');
    test.done();
  },

  changeLocale: function(test) {
    i18n.setLocale('fr');
    test.strictEqual(i18n.getLocale(), 'fr');
    test.done();
  },

  testTranslation: {
    enLocale: function(test) {
      i18n.setLocale('en');
      test.strictEqual(i18n._('Français'), 'French');
      test.done();
    },

    frLocale: function(test) {
      i18n.setLocale('fr');
      test.strictEqual(i18n._('Français'), 'Français');
      test.done();
    },

    noLocale: function(test) {
      i18n.setLocale(undefined);
      test.strictEqual(i18n._('Français'), 'Français');
      test.done();
    },

    otherLocale: function(test) {
      i18n.setLocale('ru');
      test.strictEqual(i18n._('Français'), 'Français');
      test.done();
    }
  }
};

exports.shareMessage = {
  default: function(test) {
    let req = {
      protocol: 'https',
      headers: {
	host: 'localhost'
      },
      i18n: i18n,
      args: {
	bgColor: '#f54358',
	fgColor: '#ffd201',
	text: 'démocratie',
	font: 'lineal',
	icon: 1,
	signature: ''
      }
    };
    let msg = new ShareMessage(req);
    i18n.setLocale('fr');
    test.strictEqual(msg.getUrl(), 'https://localhost/?bgColor=%23f54358&fgColor=%23ffd201&text=d%C3%A9mocratie&font=lineal&icon=1&signature=&lang=fr');
    test.strictEqual(msg.getTitle(), 'Soutenez @laquadrature ! #LQDoN');
    test.strictEqual(msg.getContent(), 'https://localhost');
    test.strictEqual(msg.getTwitterLink(), 'https://twitter.com/intent/tweet?text=Soutenez%20%40laquadrature%20!%20%23LQDoN%20https%3A%2F%2Flocalhost%20https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523f54358%26fgColor%3D%2523ffd201%26text%3Dd%25C3%25A9mocratie%26font%3Dlineal%26icon%3D1%26signature%3D%26lang%3Dfr');
    test.strictEqual(msg.getFacebookLink(), 'https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523f54358%26fgColor%3D%2523ffd201%26text%3Dd%25C3%25A9mocratie%26font%3Dlineal%26icon%3D1%26signature%3D%26lang%3Dfr');
    test.strictEqual(msg.getDiasporaLink(), 'https://share.diasporafoundation.org/?title=Soutenez%20La%20Quadrature%20du%20Net%20!%20%23LQDoN%20https%3A%2F%2Flocalhost&url=https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523f54358%26fgColor%3D%2523ffd201%26text%3Dd%25C3%25A9mocratie%26font%3Dlineal%26icon%3D1%26signature%3D%26lang%3Dfr');
    test.strictEqual(msg.getMastodonLink(), 'https://mamot.fr/share?text=Soutenez%20%40laquadrature%20!%20%23LQDoN%20https%3A%2F%2Flocalhost%20https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523f54358%26fgColor%3D%2523ffd201%26text%3Dd%25C3%25A9mocratie%26font%3Dlineal%26icon%3D1%26signature%3D%26lang%3Dfr');
    test.done();
  },

  en: function(test) {
    let req = {
      protocol: 'https',
      headers: {
	host: 'localhost'
      },
      i18n: i18n,
      args: {
	bgColor: '#f54358',
	fgColor: '#ffd201',
	text: 'démocratie',
	font: 'lineal',
	icon: 1,
	signature: ''
      }
    };
    let msg = new ShareMessage(req);
    i18n.setLocale('en');
    test.strictEqual(msg.getUrl(), 'https://localhost/?bgColor=%23f54358&fgColor=%23ffd201&text=d%C3%A9mocratie&font=lineal&icon=1&signature=&lang=en');
    test.strictEqual(msg.getTitle(), 'Support @laquadrature ! #LQDoN');
    test.strictEqual(msg.getContent(), 'https://localhost');
    test.strictEqual(msg.getTwitterLink(), 'https://twitter.com/intent/tweet?text=Support%20%40laquadrature%20!%20%23LQDoN%20https%3A%2F%2Flocalhost%20https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523f54358%26fgColor%3D%2523ffd201%26text%3Dd%25C3%25A9mocratie%26font%3Dlineal%26icon%3D1%26signature%3D%26lang%3Den');
    test.strictEqual(msg.getFacebookLink(), 'https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523f54358%26fgColor%3D%2523ffd201%26text%3Dd%25C3%25A9mocratie%26font%3Dlineal%26icon%3D1%26signature%3D%26lang%3Den');
    test.strictEqual(msg.getDiasporaLink(), 'https://share.diasporafoundation.org/?title=Support%20La%20Quadrature%20du%20Net%20!%20%23LQDoN%20https%3A%2F%2Flocalhost&url=https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523f54358%26fgColor%3D%2523ffd201%26text%3Dd%25C3%25A9mocratie%26font%3Dlineal%26icon%3D1%26signature%3D%26lang%3Den');
    test.strictEqual(msg.getMastodonLink(), 'https://mamot.fr/share?text=Support%20%40laquadrature%20!%20%23LQDoN%20https%3A%2F%2Flocalhost%20https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523f54358%26fgColor%3D%2523ffd201%26text%3Dd%25C3%25A9mocratie%26font%3Dlineal%26icon%3D1%26signature%3D%26lang%3Den');
    test.done();
  },

  advanced_fr: function(test) {
    let req = {
      protocol: 'https',
      headers: {
	host: 'localhost'
      },
      i18n: i18n,
      args: {
	bgColor: '#ff9146',
	fgColor: '#ee0088',
	text: 'pelle à tarte',
	font: 'solid-mirage',
	icon: 27,
	signature: 'nos amis'
      }
    };
    let msg = new ShareMessage(req);
    i18n.setLocale('fr');
    test.strictEqual(msg.getUrl(), 'https://localhost/?bgColor=%23ff9146&fgColor=%23ee0088&text=pelle%20%C3%A0%20tarte&font=solid-mirage&icon=27&signature=nos%20amis&lang=fr');
    test.strictEqual(msg.getTitle(), 'Faites comme nos amis, soutenez @laquadrature ! #LQDoN');
    test.strictEqual(msg.getContent(), 'https://localhost');
    test.strictEqual(msg.getTwitterLink(), 'https://twitter.com/intent/tweet?text=Faites%20comme%20nos%20amis%2C%20soutenez%20%40laquadrature%20!%20%23LQDoN%20https%3A%2F%2Flocalhost%20https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523ff9146%26fgColor%3D%2523ee0088%26text%3Dpelle%2520%25C3%25A0%2520tarte%26font%3Dsolid-mirage%26icon%3D27%26signature%3Dnos%2520amis%26lang%3Dfr');
    test.strictEqual(msg.getFacebookLink(), 'https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523ff9146%26fgColor%3D%2523ee0088%26text%3Dpelle%2520%25C3%25A0%2520tarte%26font%3Dsolid-mirage%26icon%3D27%26signature%3Dnos%2520amis%26lang%3Dfr');
    test.strictEqual(msg.getDiasporaLink(), 'https://share.diasporafoundation.org/?title=Faites%20comme%20nos%20amis%2C%20soutenez%20La%20Quadrature%20du%20Net%20!%20%23LQDoN%20https%3A%2F%2Flocalhost&url=https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523ff9146%26fgColor%3D%2523ee0088%26text%3Dpelle%2520%25C3%25A0%2520tarte%26font%3Dsolid-mirage%26icon%3D27%26signature%3Dnos%2520amis%26lang%3Dfr');
    test.strictEqual(msg.getMastodonLink(), 'https://mamot.fr/share?text=Faites%20comme%20nos%20amis%2C%20soutenez%20%40laquadrature%20!%20%23LQDoN%20https%3A%2F%2Flocalhost%20https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523ff9146%26fgColor%3D%2523ee0088%26text%3Dpelle%2520%25C3%25A0%2520tarte%26font%3Dsolid-mirage%26icon%3D27%26signature%3Dnos%2520amis%26lang%3Dfr');
    test.done();
  },

  advanced_en: function(test) {
    let req = {
      protocol: 'https',
      headers: {
	host: 'localhost'
      },
      i18n: i18n,
      args: {
	bgColor: '#ff9146',
	fgColor: '#ee0088',
	text: 'pelle à tarte',
	font: 'solid-mirage',
	icon: 27,
	signature: 'our friends'
      }
    };
    let msg = new ShareMessage(req);
    i18n.setLocale('en');
    test.strictEqual(msg.getUrl(), 'https://localhost/?bgColor=%23ff9146&fgColor=%23ee0088&text=pelle%20%C3%A0%20tarte&font=solid-mirage&icon=27&signature=our%20friends&lang=en');
    test.strictEqual(msg.getTitle(), 'Just like our friends, support @laquadrature ! #LQDoN');
    test.strictEqual(msg.getContent(), 'https://localhost');
    test.strictEqual(msg.getTwitterLink(), 'https://twitter.com/intent/tweet?text=Just%20like%20our%20friends%2C%20support%20%40laquadrature%20!%20%23LQDoN%20https%3A%2F%2Flocalhost%20https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523ff9146%26fgColor%3D%2523ee0088%26text%3Dpelle%2520%25C3%25A0%2520tarte%26font%3Dsolid-mirage%26icon%3D27%26signature%3Dour%2520friends%26lang%3Den');
    test.strictEqual(msg.getFacebookLink(), 'https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523ff9146%26fgColor%3D%2523ee0088%26text%3Dpelle%2520%25C3%25A0%2520tarte%26font%3Dsolid-mirage%26icon%3D27%26signature%3Dour%2520friends%26lang%3Den');
    test.strictEqual(msg.getDiasporaLink(), 'https://share.diasporafoundation.org/?title=Just%20like%20our%20friends%2C%20support%20La%20Quadrature%20du%20Net%20!%20%23LQDoN%20https%3A%2F%2Flocalhost&url=https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523ff9146%26fgColor%3D%2523ee0088%26text%3Dpelle%2520%25C3%25A0%2520tarte%26font%3Dsolid-mirage%26icon%3D27%26signature%3Dour%2520friends%26lang%3Den');
    test.strictEqual(msg.getMastodonLink(), 'https://mamot.fr/share?text=Just%20like%20our%20friends%2C%20support%20%40laquadrature%20!%20%23LQDoN%20https%3A%2F%2Flocalhost%20https%3A%2F%2Flocalhost%2F%3FbgColor%3D%2523ff9146%26fgColor%3D%2523ee0088%26text%3Dpelle%2520%25C3%25A0%2520tarte%26font%3Dsolid-mirage%26icon%3D27%26signature%3Dour%2520friends%26lang%3Den');
    test.done();
  }

}
